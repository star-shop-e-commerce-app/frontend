import React from 'react'

import {ListGroup, Alert, Button} from 'react-bootstrap'
import {useConversations} from '../contexts/ConversationsProvider'
import { BsFillTrashFill } from "react-icons/bs";


export default function Conversations() {

	const {conversations, selectConvoId, deleteConvo} = useConversations()

	
	return (
		<ListGroup variant="flush">
			{conversations.map((conversation, index) => {
				return (
					<div key={conversation.convoRefId} className="d-flex mt-1">
						<Button
							variant="danger"
							type="button"
							size="sm"
							className="conTab"
							onClick={()=> {deleteConvo({id:conversation.convoRefId, savedtoDb: conversation.savedtoDb})}}>
							<BsFillTrashFill/>
						</Button>
						<Button
							variant="outline-dark"
							type="button"
							className="conTab"
							block
							size="lg"
							active={conversation.selected}
							onClick={()=> {selectConvoId(conversation.convoRefId)}}
							>
							{conversation.recipients.name}
							
						</Button>
						
						
							
					</div>
				)}
			)}
			{conversations.length <1 ?<Alert variant="warning">Empty conversation</Alert> : ""}
		</ListGroup>
	)
}
