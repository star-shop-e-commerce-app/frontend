import React, { useState, useEffect } from 'react';

import {Navbar, Nav, Form, Row, Col, Image, Button, Modal, Alert, Spinner } from 'react-bootstrap';


import { useConversations } from '../contexts/ConversationsProvider'

import moment from 'moment'
import Swal from 'sweetalert2';

import { MdAddToPhotos } from "react-icons/md";

import { useHistory, Redirect} from "react-router-dom";
import categories from '../data/categories';

export default function UpdateItem ({props}) {
	

	const { numComma, indexDate, my } = useConversations()
	

	const { category, condition, createdOn, description, isActive, photos, price, subCategory, title, userId, _id, } = props.item;

	const closeModal = props.closeModal;

	const __setPhotos = props.setPhotos;
	
	const fileRef1 = React.createRef();

	let history = useHistory();


	
	const [photos_, setPhotos_] = useState([])
	
	const [photosCss, setPhotosCss] = useState('sell-2-a')
	
	const [alert , setAlert] = useState(false)
	const [alert2 , setAlert2] = useState(false)

	const [title_, setTitle_] = useState(title)
	const [price_, setPrice_] = useState(price.replace(/,/g, ""))
	const [category_, setCategory_] = useState(category)
	const [subCategory_, setSubCategory_] = useState(subCategory)
	const [condition_, setCondition_] = useState(condition)
	const [description_, setDescription_] = useState(description)

	const [itemInfo, setItemInfo] = useState('')

	
	const [disableButton, setDisableButton] = useState(false)

	const [hide, setHide] = useState(false)
	
	const dateTimeLive = new Date();

	let count = 0;


	function hideIt(x) {

		let temp = photos_.length; 		 	
		if(x>0 && count<10){ temp = temp + 1; }
	 	if(x<0){ temp = temp - 1; }
	 	if(temp>9) { setHide(true)} else {setHide(false) }
	 	if(temp>0){ setPhotosCss('sell-2-b') } else {setPhotosCss('sell-2-a') }
	}


	function cancel(element, index) {

		setPhotos_(prevPhotos=> {

			let tempData = [];

			prevPhotos.forEach(elem => {
			
				if(elem._id !== element._id) {
					tempData.push(elem)
				}
				
			})
			return tempData

		})

		hideIt(-1)

	}



	useEffect(()=>{

		setPhotos_(prevPhotos=> {

			const newPhotos = photos.map((p,index)=>{
				const data = {
					_id: p._id,
					url: p.url,
					public_id: p.public_id,
					created_at: p.created_at
				}

				return data
			})
			hideIt(1)
			return newPhotos
			
		})
		
	},[])

	useEffect(()=>{
		console.log(photos_)
	},[photos_])


	const formatted = photos_.map((element) =>{
		if(element._id!==null) {
			return{
				_id: element._id,
				url: element.url,
				public_id: element.public_id,
				created_at: element.created_at
			}
		} else {
			return{
				_id:'',
				url:'',
				public_id:'',
				created_at:''
			}
		}
	})


	const imageArr = formatted.map((element, index) => {

		if(element._id!=='') {
			return (
				<div key={index} className="sell-3-a">
					
					{disableButton ?
						<button 
						type="button"
						className="sell-3-b2" 
						disabled>X</button>
					:
						<button 
						type="button"
						className="sell-3-b1" 
						onClick={ ()=> {cancel(element, index)}}>X</button>
					}
					
					<Image
						className="sell-3-c" 
						src={`${element.url}`}/>
				</div>
			)	
		} else {
			return null
		}
	})

	

	const prepPhoto = () => {

		const reader = new FileReader();
		const file = fileRef1.current.files[0]

		if(file) { reader.readAsDataURL(file) }

		reader.onload = () => {
			
			const readerUrl = reader.result;
			
			
			let formattedPhoto = { 
				_id: 'tempid0'+indexDate,
				url: readerUrl,
				public_id:'',
				created_at: moment(dateTimeLive).format('YYYY-MM-DDTHH:mm:ssZ')
			};

			// let tempData = [];
			setPhotos_(prevPhotos=>{
				return [...prevPhotos, formattedPhoto]
			})
			
		
		}
		reader.onerror = (error) => {
			console.log('got an error while uploading', error)
		}
		
		hideIt(1)
	}

	const updateItem = (value) => {
		setDisableButton(true)
		let itemId = '';
		const setItem = props.setItem
		
		fetch(`${process.env.REACT_APP_API_URL}/item/update-item/${_id}`, { 

			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				photos: value.photos,
				title: value.title,
				price: value.price,
				category: value.category,
				subCategory: value.subCategory,
				condition: value.condition,
				description:value.description
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data){

				setDisableButton(false)

				__setPhotos(data.photos)

				setItem({
					category:data.category,
					condition:data.condition,
					createdOn:data.createdOn,
					description:data.description,
					isActive:data.isActive,
					photos:data.photos,
					price:numComma(data.price),
					subCategory:data.subCategory,
					title:data.title,
					userId:data.userId,
					_id:data._id
				})
				Swal.fire({
					position: 'center',
					icon: 'success',
					title: 'Done!',
					showConfirmButton: false,
					timer: 7000
			    })
			    
		
				// history.push(`/item/${my.id}/${_id}`)
				closeModal()
			} else {
				setDisableButton(false)
				Swal.fire({
					position: 'center',
					icon: 'warning',
					title: 'Internal Server Error. Please try again.',
					showConfirmButton: false,
					timer: 2000
			    })

			    closeModal()
			}
		})
	}


	async function editItem(e) {

		e.preventDefault()
		
		const value = {
			photos: photos_,
			title: title_,
			price: price_,
			category: category_,
			subCategory: subCategory_,
			condition: condition_,
			description:description_
		}

		if(photos_.length>0) {

			// checking if connected to the internet
			if( navigator.onLine) {

				setAlert(false)
				setAlert2(false)
				setDisableButton(true)
				// closeModal()`
				
			    await updateItem(value);
			   
			} else {
				setAlert2(true)
			}
			

		} else {
			setAlert(true)
			setDisableButton(false)
		}
	}

	const selectSubCategories = categories.map((element, idx)=>{

		if(element.category===category_){
			return( <option key={idx} value={element.subCategory}>{element.title}</option> )
		}
	})
	

	return (
		<React.Fragment>
		
		<Modal.Body>
			<Row className="my-5 bgWhite shadow" noGutters>
			<Col className="py-4">
				
				<Form onSubmit={(e) => editItem(e)}>


					
					<div className="sell-1">

						<div className={`${photosCss}`}>
							{imageArr}
							{!hide ?
								<React.Fragment>
									{disableButton ?
										null
									:
										<React.Fragment>
										<div className="sell-3-a noBorder">
											<div className="sell-3-c sell-3-d">
												<input 
													type="file" 
													id="sell-3-input"
													value={""}
													ref={fileRef1}
													accept="image/*"
													onChange= {prepPhoto}
													className="sell-3-input2"
													/>
												<label 
													htmlFor="sell-3-input" 
													id="sell-3-label"
													className="btn btn-warning"
												><MdAddToPhotos/><br/>Add photo 
												</label>
											</div>
										</div>
										</React.Fragment>
									}
								</React.Fragment>
							: null
							}
						</div>
					</div>

					<div className="sell-1-c">
					
						<p>limited to 10 photos only.</p>
					
					</div>
					


					<div className="sell-1 sell-1-b">


					
						<div className="settings-b1 settings-b1-sell mb-5">
							<input
						  		id="title"
					        	type="text" 
					        	placeholder="Title" 
					        	value={title_}
					        	onChange={e => setTitle_(e.target.value)} 
					        	className="settings-b3 settings-b3-sell"
					        	required/>
						</div>

						<div className="settings-b1 settings-b1-sell mb-5">
							
							<span 
							className="settings-b3 settings-b3-sell sell-4-price_1">PHP
							</span>
							<input
								id="price"
								type="number" 
								placeholder="Price" 
								value={price_}
					        	onChange={e => setPrice_(e.target.value)} 
					        	className="settings-b3 settings-b3-sell sell-4-price_2"
					        	required/>
						    
						</div>

						<div className="settings-b1 settings-b1-sell mb-5">
							<select 
								id="category" className="settings-b3 settings-b3-sell" 
								onChange={e => setCategory_(e.target.value)}
								defaultValue={category_}
								required>		
								<option value="">Select category...</option>
								<option value='cars_and_properties'>Cars and Properties</option>
								<option value='mobile_and_electronics'>Mobile and Electronics</option>
								<option value='jobs_and_services'>Jobs and Services</option>
								<option value='home_and_living'>Home and Living</option>
								<option value='fashion'>Fashion</option>
								<option value='hobbies_and_games'>Hobbies and Games</option>
								<option value='others'>Others</option>
					        </select>
						</div>

						{category_!==''?
							<div className="settings-b1 settings-b1-sell mb-5">
								<select 
									id="sub_category" className="settings-b3 settings-b3-sell" 
									onChange={e => setSubCategory_(e.target.value)}
									defaultValue={subCategory_}
									required>
										<option value="">Select sub-category . . .</option>
										{selectSubCategories}
								</select>
							</div>:null
						}

						<div className="settings-b1 settings-b1-sell mb-5">
							<select 
								id="condition" className="settings-b3 settings-b3-sell" 
								onChange={e => setCondition_(e.target.value)}
								defaultValue={condition_}
								required>		
								<option value="">Condition</option>
						        <option value="New">New</option>
						        <option value="Used">Used</option>
					        </select>
						</div>

						<div className="settings-b1 settings-b1-sell mb-5 textWhiteSpace">	
					        <textarea 
					        	id="description"
					        	placeholder="Description" 
					        	value={description_}
					        	onChange={e => setDescription_(e.target.value)} 
					        	className="settings-b3 settings-b3-sell"
					        	required>
					        </textarea>
						</div>

						{alert ? <Alert variant="warning" className="mt-2">Please add photo.</Alert> :null }
						{alert2 ? <Alert variant="warning" className="mt-2">Status: offline.</Alert> :null }
					

						<Row className="justify-content-end" noGutters>

							{disableButton ? 
								<Button id="mySellItemBtn2" variant="warning" type="submit" disabled>
									Uploading..
									<Spinner 
				                		animation="border" 
				                		variant="secondary"
				                		className="ml-4" 
				                		size="sm" 
				                		role="status"/>
				            	</Button>
				            	:
			                	<Button id="mySellItemBtn" variant="warning" type="submit">Submit</Button>
			                }
						</Row>
					</div>
				</Form>
			</Col>
			</Row>
		</Modal.Body>
		</React.Fragment>
	)
};
	
