import React from 'react';

import { Col, Row, Jumbotron } from 'react-bootstrap';

import { Link } from 'react-router-dom';


export default function Banner({dataProp}) {
	const {title, content, destination, label} = dataProp;


	return (

		<Row>		
			<Col>
				<Jumbotron>
					<h1>{ title }</h1>
					<p>{ content }</p>
					<Link to={destination}> 
						{label}
					</Link>

				</Jumbotron>
				
			</Col>
		</Row>
		)
}



// <Link to=""></Link> same as href, the diff is its a component now