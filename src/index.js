import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import NavBarCss from './NavBarCss';
/*import NavBar from './NavBar'; */

// import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';

// process for creating and rendering componenet
	// create a component - NavBar.js
	// import the compnnt - index.js
	// render/call the componnt - <NavBar />

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

