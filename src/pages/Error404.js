import React from 'react';
import Banner from "../components/Banner";


export default function Error404(){
	
	const data = {
		title: "404 -not found",
		content: "the page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}

	return (
	<React.Fragment>
		<Banner dataProp={data}/>
	</React.Fragment>
	)
}