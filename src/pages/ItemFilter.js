import React, { useState, useEffect, useCallback } from 'react';


import Categories from '../components/Categories';
import Item from '../components/Item';

import { Row, Col, Form, InputGroup, Button } from 'react-bootstrap';

// import UserContext from '../UserContext';
import { useGlobalData } from '../UserProvider';

import { useRouteMatch } from "react-router-dom";

import moment from 'moment'


export default function ItemFilter () {
	
	const {refreshData } = useGlobalData();
	
	const [condition, setCondition] = useState('All')

	const [new_, setNew_] = useState(false)
	const [used_, setUsed_] = useState(false)
	const [max, setMax] = useState('')
	const [min, setMin] = useState('')
	const [sortBy, setSortBy] = useState('Recent')

	const [allItems, setAllItems] = useState([])
	// const [newItems, setNewItems] = useState([])
	const [db_Length, setDb_Length] = useState(0)
	
	
	const [start, setStart] = useState(0)
	const [last, setLast] = useState(0)

	const [loadMoreBtn, setLoadMoreBtn] = useState(false)

	
	let itemLimits = 18; // number of items to display
	
	let startingIndex = 0;
	let lastIndex = 0;
	

	const match = useRouteMatch({
		path: "/item/:category",
		strict: true,
		sensitive: true
	});

		
	function goFilter (e) {
		const tempDbLen = 0;
		
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/item/category/${match.params.category}`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({ 
            	condition: condition,
            	max: max,
            	min: min,
            	db_Length: tempDbLen,
            	itemLimits: itemLimits,
                startingIndex: startingIndex,
                lastIndex: lastIndex,
                sortBy: sortBy
            })
        })
	    .then(res => res.json())
        .then(data => {

        	if(data.items.length>0){
        		setAllItems(data.items)
        		

				setDb_Length(data.len)
				setLast(0)
				setStart(0)
				if(data.len > itemLimits) {	
					setLoadMoreBtn(true) 
				} else {
					setLoadMoreBtn(false)
				}

        	} else {
        		setAllItems([])
        		setLoadMoreBtn(false)
        	}
        })
	}


	const loadMore = () => {

		if(db_Length!==0) {
			if((startingIndex===0 && lastIndex===0) && (start===0 && last ===0) ) {

				lastIndex = lastIndex + itemLimits;
				startingIndex = lastIndex + itemLimits;
				
				setLast(lastIndex)
				setStart(startingIndex)
				
			} else {
				lastIndex = (last + itemLimits);
				startingIndex = (start + itemLimits);

				setLast(lastIndex)
				setStart(startingIndex)
			}

			fetch(`${process.env.REACT_APP_API_URL}/item/category/${match.params.category}`, {
	            method: 'POST',
	            headers: {
	                'Content-Type' : 'application/json'
	            },
	            body: JSON.stringify({ 
	            	condition: condition,
	            	max: max,
	            	min: min,
	            	db_Length: db_Length,
	            	itemLimits: itemLimits,
	                startingIndex: startingIndex,
	                lastIndex: lastIndex,
	                sortBy: sortBy
	            })
	        })
		    .then(res => res.json())
	        .then(data => {

	        	if(data.items.length>0){
	        		
	        		// setNewItems(data.items)
	        		addNewItems(data.items)
	        		setDb_Length(data.len)

	        		if(startingIndex >= db_Length) {
						setLoadMoreBtn(false)
					} else {
						setLoadMoreBtn(true)
					}
	        	
	        	} 
	        })

		} else {
			console.log('something went wrong!')
		}
	}



	// adding new items to render
	// useEffect(()=>{
		
	// 	let temp = [];
	// 	let cnt = 0;
	// 	let newItemsLength = newItems.length;
	// 	if(newItemsLength>0) {
	// 		newItems.map(element => {
	// 			cnt++
	// 			temp.push(element)

	// 			if(cnt>= newItemsLength) {
					
	// 				allItems.map(element=> {
	// 					temp.push(element)
	// 				})
	// 			}
	// 		})
			
	// 	}
	// 	setAllItems(temp)

	// },[newItems])

	function addItem(item){
		
		setAllItems(previtems=> {
			return [item, ...previtems]
		})

	}

	const addNewItems = useCallback((newitems)=>{

		for(let item of newitems){
			addItem(item)
		}

	}, [])


	useEffect(()=> {

		if(match.params.category!== localStorage.getItem('prevCategory')) {
			localStorage.setItem('prevCategory', match.params.category)
			window.location.reload();
		}

		console.log('match', match)

		fetch(`${process.env.REACT_APP_API_URL}/item/category/${match.params.category}`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({ 
            	condition: condition,
            	max: max,
            	min: min,
            	db_Length: db_Length,
            	itemLimits: itemLimits,
                startingIndex: startingIndex,
                lastIndex: lastIndex,
                sortBy: sortBy
            })
        })
	    .then(res => res.json())
        .then(data => {
        	
        	if(data.items.length>0){
        		
				setAllItems(data.items)
				
				setDb_Length(data.len)

				if(data.len > itemLimits) {	setLoadMoreBtn(true) }

        	} 
        })
        
	},[refreshData])


	const itemsArray = allItems.map((element, idx) => {
		
		// if(element.isActive){
			return {
				category: element.category,
				condition: element.condition,
				createdOn: element.createdOn,
				description: element.description,
				firstName: element.firstName,
				isActive: element.isActive,
				lastName: element.lastName,
				photos: element.photos,
				price: element.price,
				subCategory: element.subCategory,
				title: element.title,
				userId: element.userId,
				_id: element._id,
				num: idx,
				date: moment(element.createdOn).format('YYYY-MM-DD'),
				price2: parseFloat(element.price)
			}
		// }
	})

	

	const	sortedItems = itemsArray.sort((a,b) => {
			if (a.num < b.num) {
				return 1 
				
			} else if (a.num > b.num) {
				return -1 
				
			} else {
				return 0
			}
		})
	
	const arrangedItems = sortedItems.map(element => {
		
		// if(element.isActive) {
			return (<Item key={element._id} dataProps={element}/>)
		// }
	})

	
	

	useEffect(()=>{
		if(new_ && !used_){
			
			setCondition('New')
			
		} else if(!new_ && used_) {
			
			setCondition('Used')
			
		} else if (!new_ && !used_) {
			
			setCondition('All')
			
		} else {
			
			setCondition('All')
			
		}

	},[new_, used_])

	useEffect(() => {
        return () => {
           
            localStorage.removeItem('prevCategory')
        }
    }, [])
	
	return (
		<React.Fragment>
			<Row className="my-5 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0 ">
					<Row className="mb-4 mt-1 px-3" noGutters>
						<Col xs={12}>
							<span  className="lgTxt">CATEGORIES</span>
						</Col>
					</Row>
					<Row noGutters>
						<Col xs={12}>
							<Categories/>
						</Col>
					</Row>
				</Col>
			</Row>

			<Row className="my-5 pb-4 bgWhite shadow" noGutters>
				<Col xs={12} className="p-0">
					
					<Row noGutters>
						<div className="b display_none pl-4 py-4">
							<Form onSubmit={(e) => goFilter(e)}>

								<Form.Group className="pr-4">
									<Form.Label className="fontsize">
											Sort by:
									</Form.Label>
									 <Form.Control 
									 	as="select" 
									 	onChange={e => setSortBy(e.target.value)} 
									 	defaultValue="Recent"
									 	>
									 	<option value="Recent">Recent</option>
								        <option value="LowestToHighest">Price: Low to High</option>
								        <option value="HighestToLowest">Price: Hight to Low</option>
								    </Form.Control>
								</Form.Group>

								<Form.Group className="mt-5">
									<Form.Label className="fontsize">
											Condition:
									</Form.Label>
									<Form.Check 
								    	type="checkbox" 
								    	id="new" 
								    	label="New"
								    	onClick={()=>{setNew_(!new_)}}/>
									<Form.Check 
										type="checkbox" 
										id="used" 
										label="Used"
										onClick={()=>{setUsed_(!used_)}}/>
								</Form.Group>

								<Form.Group className="mt-5 pr-4">
									<Form.Text className="fontsize">Price:</Form.Text>
									
									<Form.Label srOnly>
											Minimum
									</Form.Label>
									<InputGroup className="mb-2 mr-2">
										<InputGroup.Prepend>
											<InputGroup.Text>
												PHP
											</InputGroup.Text>
									    </InputGroup.Prepend>
									    <Form.Control 
									    	type="number" 
									    	className="bgWhite" 
									    	placeholder="Minimum"
									    	value={min} 
									    	onChange={e => setMin(e.target.value)} />
								    </InputGroup>

								   <Form.Label srOnly>
											Maximum
									</Form.Label>
									<InputGroup className="mb-2 mr-2">
										<InputGroup.Prepend>
									    	<InputGroup.Text>
									    		PHP
									    	</InputGroup.Text>
									    </InputGroup.Prepend>
									    <Form.Control 
									    	type="number" 
									    	className="bgWhite" 
									    	placeholder="Maximum"
									    	value={max} 
									    	onChange={e => setMax(e.target.value)} />
								    </InputGroup>
								</Form.Group>

								<Button id="button1" variant="success" size="md" type="submit">
									Apply
								</Button>

							</Form>
						</div>
						<div className="c shadow">
							<div className="urlDiv2">
								
								<div className="homeDiv1 homeDiv1_v2">
									{arrangedItems}
								</div>
								<Row className="justify-content-center mt-3" noGutters>
									
									{loadMoreBtn?
									<React.Fragment>
										<Button 
											variant="warning"
											type="button"
											onClick={loadMore}
											className=" px-5 py-1">
												LOAD MORE
										</Button>
									</React.Fragment>
									:
									null
									}
								</Row>
							</div>
						</div>
					</Row>
				</Col>
			</Row>

			
		
		</React.Fragment>
	)
};

