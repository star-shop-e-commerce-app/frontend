import { useEffect} from 'react';

// import UserContext from '../UserContext';
import { useGlobalData } from '../UserProvider';


import { useHistory } from 'react-router-dom';


export default function Logout() {
	const {unsetUser} = useGlobalData();

	
	let history = useHistory();
	
	useEffect (()=> {
		unsetUser();	
		history.push('/');
		// window.location.reload();
		window.location.assign("/");
	});
	
	return null
};