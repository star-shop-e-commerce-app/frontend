import React, { useState } from 'react';
import { Form, Row, Col, Image, Button, Alert, Spinner } from 'react-bootstrap';

import categories from '../data/categories';

import moment from 'moment'
import Swal from 'sweetalert2';

import { MdAddToPhotos } from "react-icons/md";

import { useHistory, Redirect} from "react-router-dom";

export default function Sell () {
	
	const fileRef1 = React.createRef();

	let history = useHistory();
	
	
	const [photos, setPhotos] = useState([])
	
	
	const [photosCss, setPhotosCss] = useState('sell-2-a')
	
	const [alert , setAlert] = useState(false)
	const [alert2 , setAlert2] = useState(false)

	const [title, setTitle] = useState('')
	const [price, setPrice] = useState('')
	const [category, setCategory] = useState('')
	const [subCategory, setSubCategory] = useState('')
	const [condition, setCondition] = useState('')
	const [description, setDescription] = useState('')


	const [disableButton, setDisableButton] = useState(false)

	const [hide, setHide] = useState(false)
	
	const dateTimeLive = new Date();

	
	let count = 0;

	function hideIt(x) {

		let temp = photos.length; 		 	
		if(x>0 && count<10){ temp = temp + 1; }
	 	if(x<0){ temp = temp - 1; }
	 	if(temp>9) { setHide(true)} else {setHide(false) }
	 	if(temp>0){ setPhotosCss('sell-2-b') } else {setPhotosCss('sell-2-a') }
	}


	function cancel(element, index) {
		
		const tempData = [];

		photos.forEach(element => {
			
			if(element.date !== photos[index].date) {
				tempData.push(element)
			}
			setPhotos(tempData)
		})
		hideIt(-1)

	}

	const forSorting = photos.map((element) =>{
		if(element.date!==null) {
			return{
				_date: parseInt(moment(element.date).format('YYYYMMDDHHmmss')),
				date: element.date,
				data: element.data,
				dataId:element.dateId
			}
		} else {
			return{
				_date: 0,
				date: '',
				data: '',
				dataId:''
			}
		}
	})

	const sorted = forSorting.sort((a,b) => {
		if (a._date < b._date) {
			return -1
			
		} else if (a._date > b._date) {
			return 1
			
		} else {
			return 0
		}
	})

	const imageArr = sorted.map((element, index) => {

		if(element.data!=='' && element._date !==0) {
			return (
				<div key={index} className="sell-3-a">
					
					{disableButton ?
						<button 
						type="button"
						className="sell-3-b2" 
						disabled>X</button>
					:
						<button 
						type="button"
						className="sell-3-b1" 
						onClick={ ()=> {cancel(element, index)}}>X</button>
					}
					
					<Image
						className="sell-3-c" 
						src={`${element.data}`}/>
				</div>
			)	
		} else {
			return null
		}
	})


	


	const uploadPhoto3 = () => {
		
		const reader = new FileReader();
		const file = fileRef1.current.files[0]

		if(file) { reader.readAsDataURL(file) }

		reader.onload = () => {
			const readerUrl = reader.result;
			let tempVar = {
				dataId:moment(dateTimeLive).format('YYYYMMDDTHHmmsssssZ'), 
				date:dateTimeLive, 
				data: readerUrl
			};

			let tempData = [];
			
			if(photos.length > 0) {
				
				photos.forEach(element => {
					tempData.push(element)
				})
				tempData.push(tempVar)
				setPhotos(tempData)

			} else {
				tempData.push(tempVar)
				setPhotos(tempData)	
			}	
		}
		reader.onerror = (error) => {
			console.log('got an error while uploading', error)
		}
		hideIt(1)
	}

	const uploadPhoto_1 = async (value) => {
		
		setDisableButton(true)
		let itemId = '';
		
		await fetch(`${process.env.REACT_APP_API_URL}/item/upload`, { 

			method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                photos: value.photos,
                title: value.title,
				price: value.price,
				category: value.category,
				subCategory: value.subCategory,
				condition: value.condition,
				description:value.description
            })
        })
        .then(res => res.json())
        .then(data => {
        	
        	if(data.success){
	        	
	        	itemId = data.itemInfo._id;
	        	setDisableButton(false)
	        	localStorage.setItem('itemId_', itemId)
	        	history.push(`/item/${localStorage.getItem('userId')}/${localStorage.getItem('itemId_')}`)
	        } else {

	        	setDisableButton(false)
	        	Swal.fire({
                  position: 'center',
                  icon: 'warning',
                  title: 'Internal Server Error. Please try again.',
                  showConfirmButton: false,
                  timer: 7000
                })
               
	        }
        })
	}


	async function sellItem(e) {
		e.preventDefault()
		

		const value = {
			photos: photos,
			title: title,
			price: price,
			category: category,
			subCategory: subCategory,
			condition: condition,
			description:description
		}

		if(photos.length>0) {

		
			if( navigator.onLine) {

				setAlert(false)
				setAlert2(false)
				setDisableButton(true)
				
			    await uploadPhoto_1(value);
			   
			} else {
				setAlert2(true)
			}
		} else {
			
			setAlert(true)
			setDisableButton(false)
		}
	}

	const selectSubCategories = categories.map((element, idx)=>{

		if(element.category===category){
			return( <option key={idx} value={element.subCategory}>{element.title}</option> )
		}
	})

	return (
		<React.Fragment>	
			{ localStorage.getItem('email') !== null && localStorage.getItem('email') !== 'undefined' ?
			<Row className="my-5 bgWhite shadow" noGutters>
			<Col xs={12} className="py-4">
				<Form onSubmit={(e) => sellItem(e)}>
					<div className="sell-1">
						<div className={`${photosCss}`}>
							{imageArr}
							{!hide ?
								<React.Fragment>
									{disableButton ?
										null
									:
										<React.Fragment>
										<div className="sell-3-a noBorder">
											<div className="sell-3-c sell-3-d">
												<input 
													type="file" 
													id="sell-3-input"
													value={""}
													ref={fileRef1}
													accept="image/*"
													onChange= {uploadPhoto3}
													className="sell-3-input2"
													/>
												<label 
													htmlFor="sell-3-input" 
													id="sell-3-label"
													className="btn btn-warning"
												><MdAddToPhotos/><br/>Add photo 
												</label>
											</div>
										</div>
										</React.Fragment>
									}
								</React.Fragment>
							: null
							}
						</div>
					</div>
					<div className="sell-1-c">
					
						<p>limited to 10 photos only.</p>
					
					</div>

					<div className="sell-1 sell-1-b">

						<div className="settings-b1 settings-b1-sell mb-5">
							<input
						  		id="title"
					        	type="text" 
					        	placeholder="Title" 
					        	value={title}
					        	onChange={e => setTitle(e.target.value)} 
					        	className="settings-b3 settings-b3-sell"
					        	required/>
						</div>

						<div className="settings-b1 settings-b1-sell mb-5">
							
							<span 
							className="settings-b3 settings-b3-sell sell-4-price_1">PHP
							</span>
							<input
								id="price"
								type="number" 
								placeholder="Price" 
								value={price}
					        	onChange={e => setPrice(e.target.value)} 
					        	className="settings-b3 settings-b3-sell sell-4-price_2"
					        	required/>
						    
						</div>

						<div className="settings-b1 settings-b1-sell mb-5">
							<select 
								id="category" className="settings-b3 settings-b3-sell" 
								onChange={e => setCategory(e.target.value)}
								defaultValue={"Category"}
								required>		
								<option value="">Select category...</option>
								<option value='cars_and_properties'>Cars and Properties</option>
								<option value='mobile_and_electronics'>Mobile and Electronics</option>
								<option value='jobs_and_services'>Jobs and Services</option>
								<option value='home_and_living'>Home and Living</option>
								<option value='fashion'>Fashion</option>
								<option value='hobbies_and_games'>Hobbies and Games</option>
								<option value='others'>Others</option>
					        </select>
						</div>

						{category!==''?
							<div className="settings-b1 settings-b1-sell mb-5">
								<select 
									id="sub_category" className="settings-b3 settings-b3-sell" 
									onChange={e => setSubCategory(e.target.value)}
									defaultValue={"Sub Category"}
									required>
										<option value="">Select sub-category . . .</option>
										{selectSubCategories}
								</select>
							</div>:null
						}

						<div className="settings-b1 settings-b1-sell mb-5">
							<select 
								id="condition" className="settings-b3 settings-b3-sell" 
								onChange={e => setCondition(e.target.value)}
								defaultValue={"Condition"}
								required>		
								<option value="">Condition</option>
						        <option value="New">New</option>
						        <option value="Used">Used</option>
					        </select>
						</div>

						<div className="settings-b1 settings-b1-sell mb-5 textWhiteSpace">	
					        <textarea 
					        	id="description"
					        	placeholder="Description" 
					        	value={description}
					        	onChange={e => setDescription(e.target.value)} 
					        	className="settings-b3 settings-b3-sell"
					        	required>
					        </textarea>
						</div>
						{alert ?
							/*<p className="alert settings-b1-sell">Please add photo.</p>*/
							<Alert variant="warning" className="mt-2">Please add photo.</Alert>
							:
							null
						}
						{alert2 ?
							/*<p className="alert settings-b1-sell">Please add photo.</p>*/
							<Alert variant="warning" className="mt-2">Status: offline.</Alert>
							:
							null
						}
						<Row className="justify-content-end" noGutters>

						{disableButton ? 
							<Button id="mySellItemBtn2" variant="warning" type="submit" disabled>
								Uploading..
								<Spinner 
			                		animation="border" 
			                		variant="secondary"
			                		className="ml-4" 
			                		size="sm" 
			                		role="status"/>
			            	</Button>
			            :
		                	<Button id="mySellItemBtn" variant="warning" type="submit">Submit</Button>
		                }
						</Row>
						<div className="sell-1-c">

						</div>
					</div>
				</Form>
			</Col>
			</Row>
			:
			<Redirect to ='/'/>
			}
		</React.Fragment>
	)
};
	
