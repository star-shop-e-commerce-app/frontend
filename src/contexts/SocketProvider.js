import React, { useContext, useEffect, useState } from 'react'
import io from 'socket.io-client'

const SocketContext = React.createContext()



export function useSocket() {

  return useContext(SocketContext)
}

export function SocketProvider({ children }) {
  const [socket, setSocket] = useState()

  const id = localStorage.getItem('userId');

  useEffect(() => {
    const newSocket = io(
      `${process.env.REACT_APP_API_URL_SOCKET}`,
      { query: { id } }
    )
    setSocket(newSocket)

    // close the old socket and create a new one
    return () => newSocket.close()
  }, [id])

  return (
    <SocketContext.Provider value={socket}>
      {children}
    </SocketContext.Provider>
  )
}
