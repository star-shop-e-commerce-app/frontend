import React from 'react';


import './css/App.css';
import './css/NavBar.css';
import './css/Dropdown.css';

import NavBar from './components/NavBar';
import Footers from './components/Footers';
import Home from './pages/Home';
import Search from './pages/Search';
import Logout from './pages/Logout';
import Error404 from './pages/Error404';
import Sell from './pages/Sell';
import Chat from './pages/Chat';

import ItemFilter from './pages/ItemFilter';
import ItemDetails from './pages/ItemDetails';
import UserProfile from './pages/UserProfile';
import Settings from './pages/Settings';

		
import { UserProvider } from './UserProvider';
import { ConversationsProvider } from './contexts/ConversationsProvider'
import { SocketProvider } from './contexts/SocketProvider'

// 'as' - alias
import { BrowserRouter as Router } from 'react-router-dom';

import { Route, Switch} from 'react-router-dom';





function App() {
	
  return (
    <React.Fragment>
    	<UserProvider>
		<SocketProvider> 
		<ConversationsProvider>
		    <Router>
		    <div className="bodyDIV">
			    <NavBar/>
		   		<Switch>
		   			<Route exact path="/" component={Home}/>
		   			<Route exact path="/search/keyword=:keyword" component={Search}/>

		   			<Route exact path="/item/:category" component={ItemFilter}/>
		   			
		   			<Route exact path="/item/:userId/:itemId" component={ItemDetails}/>
		   			
		   			<Route exact path="/user/:userId" component={UserProfile}/>
		   			
		   			<Route exact path="/profile/settings" component={Settings}/>

		   			<Route exact path="/sell-item" component={Sell}/>

		   			<Route exact path="/chat" component={Chat}/>

		   			<Route exact path="/logout" component={Logout}/>
		   		
		   			<Route component={Error404}/>
		   			
		   		</Switch>
		   		<Footers/>
			</div>
			
			</Router>
		</ConversationsProvider>
	    </SocketProvider>
		</UserProvider>
    </React.Fragment>
  );
}
 
export default App;
